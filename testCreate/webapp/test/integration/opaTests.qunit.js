/* global QUnit */
QUnit.config.autostart = false;

sap.ui.getCore().attachInit(function () {
	"use strict";

	sap.ui.require([
		"testCreate/testCreate/test/integration/AllJourneys"
	], function () {
		QUnit.start();
	});
});